package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var hookInvokeCmd = &cobra.Command{
	Use:   "invoke",
	Short: "invoke a specified hook",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			fmt.Println("please specify a hook to invoke")
			return
		}
		fmt.Printf("invoking hook %s", args[0])
	},
}
