// Copyright © 2018 Conny Karlsson <connykarlsson9@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/inquizarus/hkdctl/internal"
	"gitlab.com/inquizarus/hkdctl/pkg/api"
)

func makeHookListCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "list",
		Short: "list all hooks",
		Long:  `List all hooks on the remote hkd server`,
		Run: func(cmd *cobra.Command, args []string) {
			logger := makeLoggerWrapper(isDebugEnabled(cmd), os.Stdout)
			profileData := viper.Get(profile).(map[string]string)

			if 0 == len(profileData) {
				logger.Infof("could not locate profile with name %s, does it exist in the config?", profile)
				return
			}

			p := internal.ExtractProfileFromMap(profile, profileData)

			logger.Debugf("using profile %s, target %s:%s with token %s", p.Name, p.Host, p.Port, p.Token)

			restAPI := api.NewRestWrapper(http.DefaultClient, p, logger)
			hooks, err := restAPI.Hooks().All()

			if nil != err {
				logger.Debug(err)
			}

			table := internal.Table{
				Headers: []string{
					fmt.Sprintf("%-50s", "ID"),
					fmt.Sprintf("%-50s", "secret"),
					fmt.Sprintf("%s", "allowed-origins"),
				},
			}

			for _, h := range hooks {
				table.Rows = append(table.Rows, []string{
					fmt.Sprintf("%-50v", h.Identifier),
					fmt.Sprintf("%-50v", h.Secret),
					fmt.Sprintf("%s", strings.Join(h.AllowedOrigins, ",")),
				})
			}

			table.Print(os.Stdout)
		},
	}
}
