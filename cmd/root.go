// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"io"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/inquizarus/hkdctl/pkg/log"
)

var cfgFile string
var profile string
var debug = false

func makeRootCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "hkdctl",
		Short: "Interacts with remote hkd installations",
		Long: `hkdctl Is an application that make It easier to interact
	with remote hkd installations`,
	}
}

var rootCmd = makeRootCmd()

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(func() {
		initConfig()
	})

	rootCmd.PersistentFlags().BoolVarP(&debug, "debug", "d", false, "enable debug logging")
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.hkd/ctl.json)")
	rootCmd.PersistentFlags().StringVar(&profile, "profile", "default", "profile to use in config file")

	addCommands(rootCmd)
}

func addCommands(rootCmd *cobra.Command) {
	logger := makeLoggerWrapper(viper.GetBool("debug"), os.Stdout)

	hookCmd.AddCommand(makeHookListCmd())
	hookCmd.AddCommand(hookInvokeCmd)

	computeCmd := makeComputeCmd(logger)

	computeCmd.AddCommand(hookCmd)
	rootCmd.AddCommand(computeCmd)
}

func initConfig() {
	viper.SetConfigType("json")
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		viper.AddConfigPath(home + "/.hkd")
		viper.SetConfigName("ctl")
	}

	viper.SetEnvPrefix("HKD")
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		//logger.Debugf("Using config file: %s", viper.ConfigFileUsed())
	}

}

func isDebugEnabled(cmd *cobra.Command) bool {
	f, _ := cmd.Flags().GetBool("debug")
	return f
}

func makeLoggerWrapper(debug bool, w io.Writer) log.Logger {
	logger := logrus.New()
	logger.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
		ForceColors:   true,
	})
	logger.SetOutput(w)

	if debug {
		logger.SetLevel(logrus.DebugLevel)
	}

	return log.Logger{
		Info: func(args ...interface{}) {
			logger.Info(args...)
		},
		Infof: func(msg string, args ...interface{}) {
			logger.Infof(msg, args...)
		},
		Debug: func(args ...interface{}) {
			logger.Debug(args...)
		},
		Debugf: func(msg string, args ...interface{}) {
			logger.Debugf(msg, args...)
		},
		Error: func(args ...interface{}) {
			logger.Error(args...)
		},
		Errorf: func(msg string, args ...interface{}) {
			logger.Errorf(msg, args...)
		},
	}

}
