package response

import (
	"gitlab.com/inquizarus/hkdctl/model"
)

// HookList response when fetching a list of webhooks
type HookList struct {
	Message string       `json:"message"`
	Hooks   []model.Hook `json:"webhooks"`
}
