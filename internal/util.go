package internal

import (
	"fmt"
	"io"
	"os"
	"text/tabwriter"

	"gitlab.com/inquizarus/hkdctl/model"
)

// Printable defines something that will be printed?
type Printable interface {
	Print(io.Writer)
}

// Table struct for containg tables to print
type Table struct {
	Headers []string
	Rows    [][]string
}

// Print implementation for Table
func (t Table) Print(w io.Writer) {
	tw := tabwriter.NewWriter(w, 0, 0, 1, ' ', 0)
	l := ""
	for _, h := range t.Headers {
		l = fmt.Sprintf("%s%s\t", l, h)
	}
	fmt.Fprintln(tw, l)
	for _, r := range t.Rows {
		l = ""
		for _, c := range r {
			l = fmt.Sprintf("%s%s\t", l, c)
		}
		fmt.Fprintln(tw, l)
	}
	tw.Flush()
}

// ExtractProfileFromMap filters out the correct profile if found else returns an error
func ExtractProfileFromMap(name string, data map[string]string) model.Profile {
	return model.Profile{
		Name:  name,
		Host:  getWithFallback("host", data, ""),
		Port:  getWithFallback("port", data, "8080"),
		Token: getWithFallback("token", data, ""),
	}
}

func getWithFallback(name string, mp map[string]string, fb string) string {
	if v, ok := mp[name]; ok == true && "" != v {
		return v
	}
	return fb
}

// PrintTableRow just does what it says on the can
func PrintTableRow(items ...string) {
	// Observe how the b's and the d's, despite appearing in the
	// second cell of each line, belong to different columns.
	w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
	l := ""
	for _, v := range items {
		l = fmt.Sprintf("%s%s\t", l, v)
	}
	fmt.Fprintln(w, l)
	w.Flush()
}
