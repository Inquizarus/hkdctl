package internal_test

import (
	"reflect"
	"testing"

	"gitlab.com/inquizarus/hkdctl/internal"
	"gitlab.com/inquizarus/hkdctl/model"
)

func TestThatTheFilteredOutProfileHaveCorrectValues(t *testing.T) {

	expected := model.Profile{
		Name:  "test-profile",
		Host:  "hostname.test",
		Port:  "8080",
		Token: "abc123",
	}

	data := map[string]string{
		"host":  expected.Host,
		"token": expected.Token,
	}

	profile := internal.ExtractProfileFromMap(expected.Name, data)

	isSame := reflect.DeepEqual(expected, profile)

	if !isSame {
		t.Errorf("extracted profile does not match the expected one, wanted %v but got %v", expected, profile)
	}

}
