package model

// Hook ...
type Hook struct {
	Identifier     string   `json:"identifier"`
	Name           string   `json:"name"`
	Description    string   `json:"description"`
	Secret         string   `json:"secret"`
	AllowedOrigins []string `json:"allowed-origins"`
}
