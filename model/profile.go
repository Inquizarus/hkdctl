package model

// Profile contains information about communicating with a hkd server
type Profile struct {
	Name  string
	Host  string
	Port  string
	Token string
}
