package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/inquizarus/hkdctl/model"
	"gitlab.com/inquizarus/hkdctl/pkg/log"
)

// Hooks defines the interface for interacting with remote hooks
type Hooks interface {
	All() ([]model.Hook, error)
}

// RestHooks utilize a standard remote REST api for handling hooks
type RestHooks struct {
	client  *http.Client
	logger  log.Logger
	profile model.Profile
}

// All implementation for RestHooks
func (hks *RestHooks) All() ([]model.Hook, error) {
	var hooks []model.Hook
	var err error
	hks.logger.Debug("retrieving all hooks from remote server")
	url := fmt.Sprintf("http://%s:%s/i/_all", hks.profile.Host, hks.profile.Port)
	hks.logger.Debugf("target url %s", url)
	req, err := http.NewRequest("GET", url, nil)
	if nil != err {
		return hooks, fmt.Errorf("could not initialize request, \"%s\"", err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", hks.profile.Token))
	req.Header.Set("Content-Type", "application/json")
	res, err := hks.client.Do(req)
	if nil != err {
		return hooks, fmt.Errorf("could not perform request, \"%s\"", err)
	}
	defer res.Body.Close()
	rc, err := ioutil.ReadAll(res.Body)
	if nil != err {
		return hooks, fmt.Errorf("could not handle response, \"%s\"", err)
	}
	hks.logger.Debugf("response payload: \"%s\"", string(rc))
	var response struct {
		Message string       `json:"message"`
		Hooks   []model.Hook `json:"webhooks"`
	}
	json.Unmarshal(rc, &response)
	hooks = response.Hooks
	return hooks, err
}

// NewRestHooks returns a preconfigured struct
func NewRestHooks(client *http.Client, profile model.Profile, logger log.Logger) Hooks {
	return &RestHooks{
		logger:  logger,
		client:  client,
		profile: profile,
	}
}
