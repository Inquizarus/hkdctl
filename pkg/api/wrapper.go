package api

import (
	"net/http"

	"gitlab.com/inquizarus/hkdctl/model"
	"gitlab.com/inquizarus/hkdctl/pkg/log"
)

// Wrapper defines the interface for a wrapper container of API implementations
type Wrapper interface {
	Hooks() Hooks
}

// RestWrapper for Rest based API interactions
type RestWrapper struct {
	hooks Hooks
}

// Hooks implementation for RestWrapper
func (wr *RestWrapper) Hooks() Hooks {
	return wr.hooks
}

// NewRestWrapper returns configured struct of RestWrapper
func NewRestWrapper(client *http.Client, profile model.Profile, logger log.Logger) Wrapper {
	return &RestWrapper{
		hooks: NewRestHooks(client, profile, logger),
	}
}
