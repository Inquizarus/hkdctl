package log

type Info func(args ...interface{})
type Infof func(string, ...interface{})
type Error func(args ...interface{})
type Errorf func(string, ...interface{})
type Debug func(args ...interface{})
type Debugf func(string, ...interface{})

type Logger struct {
	Info
	Infof
	Error
	Errorf
	Debug
	Debugf
}
